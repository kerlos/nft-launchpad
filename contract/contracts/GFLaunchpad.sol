//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/utils/cryptography/MerkleProof.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/access/AccessControlEnumerable.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "./interfaces/IGuyFriendNFT.sol";

contract GFLaunchpad is AccessControl {
    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");
    bytes32 private merkleRoot;
    uint256 public startDate;
    uint256 public endDate;

    //Token
    mapping(address => uint256) tokenAllocation;
    mapping(address => uint256) tokenClaimed;
    address public tokenAddress;
    uint256 public tokenPrice;
    event PurchaseToken(address indexed buyer, uint256 amountReceived);

    //NFT
    mapping(address => uint256) nftAllocation;
    mapping(address => uint256) nftClaimed;
    address public nftAddress;
    uint256 public nftPrice;
    event PurchaseNFT(address indexed buyer, uint256[] tokenIds);

    constructor(
        address _tokenAddress,
        address _nftAddress,
        uint256 _startDate,
        uint256 _endDate,
        uint256 _tokenPrice,
        uint256 _nftPrice
    ) {
        require(_tokenAddress != address(0));
        require(_nftAddress != address(0));
        require(_startDate < _endDate, "end date must be latter.");
        require(_tokenPrice > 0);
        require(_nftPrice > 0);

        _setupRole(MINTER_ROLE, msg.sender);
        _setupRole(DEFAULT_ADMIN_ROLE, msg.sender);
        tokenAddress = _tokenAddress;
        nftAddress = _nftAddress;

        startDate = _startDate;
        endDate = _endDate;
        tokenPrice = _tokenPrice;
        nftPrice = _nftPrice;
    }

    modifier onlySaleTime() {
        require(startDate < block.timestamp, "Sale not start yet.");
        require(endDate > block.timestamp, "Sale ended.");
        _;
    }

    function setSaleDate(uint256 _startDate, uint256 _endDate)
        external
        onlyRole(DEFAULT_ADMIN_ROLE)
    {
        require(_startDate < _endDate, "end date must be latter.");
        startDate = _startDate;
        endDate = _endDate;
    }

    function setMerkleRoot(bytes32 root) external onlyRole(DEFAULT_ADMIN_ROLE) {
        merkleRoot = root;
    }

    //Token
    function buyToken(bytes32[] calldata proof) external payable onlySaleTime {
        require(msg.value > 0, "Must send ethers.");

        bytes32 leaf = keccak256(abi.encodePacked(msg.sender));
        require(MerkleProof.verify(proof, merkleRoot, leaf), "Invalid Proof.");

        ERC20 tokenContract = ERC20(tokenAddress);
        uint256 payAmount = msg.value;
        require(
            tokenClaimed[msg.sender] + payAmount <= tokenAllocation[msg.sender],
            "Allocation limited."
        );

        uint256 tokenAmount = (payAmount / tokenPrice) * 10**tokenContract.decimals();

        require(
            tokenContract.balanceOf(address(this)) > tokenAmount,
            "amount exceed supply"
        );

        

        tokenContract.transfer(msg.sender, tokenAmount);
        tokenClaimed[msg.sender] += payAmount;

        emit PurchaseToken(msg.sender, tokenAmount);
    }

    function tokenAllocationOf(address addr) external view returns (uint256) {
        return tokenAllocation[addr];
    }

    function tokenClaimedOf(address addr) external view returns (uint256) {
        return tokenClaimed[addr];
    }

    function setTokenPrice(uint256 price)
        external
        onlyRole(DEFAULT_ADMIN_ROLE)
    {
        require(price > 0);
        tokenPrice = price;
    }

    function setTokenAddress(address addr)
        external
        onlyRole(DEFAULT_ADMIN_ROLE)
    {
        require(addr != address(0));
        tokenAddress = addr;
    }

    function setTokenAllocation(address addr, uint256 amount)
        external
        onlyRole(DEFAULT_ADMIN_ROLE)
    {
        tokenAllocation[addr] = amount;
    }

    function batchSetTokenAllocation(
        address[] calldata addrs,
        uint256[] calldata amounts
    ) external onlyRole(DEFAULT_ADMIN_ROLE) {
        require(
            addrs.length == amounts.length,
            "addrs length must equal amounts length"
        );
        for (uint256 i = 0; i < addrs.length; i++) {
            tokenAllocation[addrs[i]] = amounts[i];
        }
    }

    //NFT
    function buyNft(bytes32[] calldata proof, uint256 amount)
        external
        payable
        onlySaleTime
    {
        require(amount > 0, "amount required.");
        require(msg.value > 0, "Must send ethers.");

        bytes32 leaf = keccak256(abi.encodePacked(msg.sender));
        require(MerkleProof.verify(proof, merkleRoot, leaf), "Invalid Proof.");

        require(msg.value == nftPrice * amount, "send mismatch ethers");
        require(
            nftClaimed[msg.sender] + amount <= nftAllocation[msg.sender],
            "Allocation limited."
        );

        

        IGuyFriendNFT nftContract = IGuyFriendNFT(nftAddress);
        uint256[] memory tokenIds = new uint256[](amount);
        for (uint256 i = 0; i < amount; i++) {
            uint256 tokenId = nftContract.safeMint(msg.sender);
            tokenIds[i] = tokenId;
        }

        nftClaimed[msg.sender] += amount;
        emit PurchaseNFT(msg.sender, tokenIds);
    }

    function nftAllocationOf(address addr) external view returns (uint256) {
        return nftAllocation[addr];
    }

    function nftClaimedOf(address addr) external view returns (uint256) {
        return nftClaimed[addr];
    }

    function setNftPrice(uint256 price) external onlyRole(DEFAULT_ADMIN_ROLE) {
        require(price > 0);
        nftPrice = price;
    }

    function setNftAddress(address addr) external onlyRole(DEFAULT_ADMIN_ROLE) {
        require(addr != address(0));
        nftAddress = addr;
    }

    function setNftAllocation(address addr, uint256 amount)
        external
        onlyRole(DEFAULT_ADMIN_ROLE)
    {
        nftAllocation[addr] = amount;
    }

    function batchSetNftAllocation(
        address[] calldata addrs,
        uint256[] calldata amounts
    ) external onlyRole(DEFAULT_ADMIN_ROLE) {
        require(
            addrs.length == amounts.length,
            "addrs length must equal amounts length"
        );
        for (uint256 i = 0; i < addrs.length; i++) {
            nftAllocation[addrs[i]] = amounts[i];
        }
    }
}
