// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

interface IGuyFriendNFT {
    function safeMint(address to) external returns(uint256);
}