// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
const hre = require("hardhat");
const { utils } = require("ethers");
const keccak256 = require('keccak256')

async function main () {
  const Token = await ethers.getContractFactory("GuyFriendToken")
  const Nft = await ethers.getContractFactory("GuyFriendNFT")
  const Launchpad = await ethers.getContractFactory("GFLaunchpad")

  tokenContract = await Token.deploy();
  nftContract = await Nft.deploy();

  await Promise.all([nftContract.deployed(), tokenContract.deployed()])

  const startDate = new Date()
  startDate.setDate(startDate.getDate() - 1)
  const endDate = new Date()
  endDate.setDate(endDate.getDate() + 1)

  const tokenPrice = utils.parseEther("0.001")
  const nftPrice = utils.parseEther("0.01")

  lpContract = await Launchpad.deploy(
    tokenContract.address,
    nftContract.address,
    Math.floor(startDate.getTime() / 1000),
    Math.floor(endDate.getTime() / 1000),
    tokenPrice,
    nftPrice
  );

  await lpContract.deployed()


  //set MINTER_ROLE
  const minterRole = keccak256("MINTER_ROLE")
  await nftContract.grantRole(minterRole, lpContract.address)

  console.log("tokenContract deployed to:", tokenContract.address);
  console.log("nftContract deployed to:", nftContract.address);
  console.log("lpContract deployed to:", lpContract.address);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
