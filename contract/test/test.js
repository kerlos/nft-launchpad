const { expect } = require("chai");
const { utils, BigNumber } = require("ethers");
const { ethers } = require("hardhat");
const { MerkleTree } = require('merkletreejs')
const keccak256 = require('keccak256')

const nftPrice = utils.parseEther("0.2") //  0.2 ethers
const tokenPrice = utils.parseEther("0.03"); // 0.03 ethers

describe("GuyFriend Contracts", function () {
  let tokenContract;
  let nftContract;
  let lpContract;
  let owner, account1, account2, account3, account4, account5;
  let tree;
  let root

  beforeEach(async function () {
    [owner, account1, account2, account3, account4, account5] = await ethers.getSigners();

    const leaves = [owner, account1, account2].map(x => keccak256(x.address))

    tree = new MerkleTree(leaves, keccak256, { sort: true })
    root = tree.getHexRoot()

    const Token = await ethers.getContractFactory("GuyFriendToken")
    const Nft = await ethers.getContractFactory("GuyFriendNFT")
    const Launchpad = await ethers.getContractFactory("GFLaunchpad")

    tokenContract = await Token.deploy();
    nftContract = await Nft.deploy();

    await Promise.all([nftContract.deployed(), tokenContract.deployed()])

    const startDate = new Date()
    startDate.setDate(startDate.getDate() - 1)
    const endDate = new Date()
    endDate.setDate(endDate.getDate() + 1)

    lpContract = await Launchpad.deploy(
      tokenContract.address,
      nftContract.address,
      Math.floor(startDate.getTime() / 1000),
      Math.floor(endDate.getTime() / 1000),
      tokenPrice,
      nftPrice
    );

    await lpContract.deployed()

    //set root
    await lpContract.setMerkleRoot(root)

    //set MINTER_ROLE
    const minterRole = keccak256("MINTER_ROLE")
    await nftContract.grantRole(minterRole, lpContract.address)
  });

  describe("Deployment", function () {
    it("Launchpad Should deployed with right assosiated addresses", async function () {
      expect(await lpContract.tokenAddress()).to.equal(tokenContract.address)
      expect(await lpContract.nftAddress()).to.equal(nftContract.address)
    });
  })

  describe("Token contract", function () {
    it("Should have 1,000,000 total supply", async function () {
      expect(await tokenContract.totalSupply()).to.equal("1000000000000000000000000")
    })

    it("Should sent all token to owner", async function () {
      expect(await tokenContract.balanceOf(owner.address)).to.equal(await tokenContract.totalSupply())
    })

    it("Should be able to transfer", async function () {
      await tokenContract.transfer(account1.address, "100000000000000000000")
      expect(await tokenContract.balanceOf(owner.address)).to.equal("999900000000000000000000")
      expect(await tokenContract.balanceOf(account1.address)).to.equal("100000000000000000000")
    })

    it("Shuuldn't be able to transfer exceed balance", async function () {
      await expect(tokenContract.connect(account1).transfer(account2.address, "1")).to.be.reverted
    })
  })

  describe("NFT contract", function () {
    it("Should be able to mint by MINTER_ROLE account", async function () {
      expect(await nftContract.balanceOf(owner.address)).to.equal("0")

      await nftContract.safeMint(owner.address)
      expect(await nftContract.balanceOf(owner.address)).to.equal("1")

      await nftContract.safeMint(owner.address)
      expect(await nftContract.balanceOf(owner.address)).to.equal("2")

      await nftContract.safeMint(account1.address)
      expect(await nftContract.balanceOf(account1.address)).to.equal("1")
    })

    it("Shouldn't be able to mint by other accounts", async function () {
      await expect(nftContract.connect(account1).safeMint(account1.address)).to.be.reverted
    })

    it("Should be able to transfer", async function () {
      await nftContract.safeMint(owner.address)
      expect(await nftContract.ownerOf(0)).to.equal(owner.address)

      await nftContract.transferFrom(owner.address, account1.address, 0)
      expect(await nftContract.ownerOf(0)).to.equal(account1.address)
    })
  })

  describe("Launchpad contract", function () {
    it("Whitelisted should be able to buy token within allocation limit", async function () {
      //send token to Launchpad
      await tokenContract.transfer(lpContract.address, await tokenContract.balanceOf(owner.address))

      //account1 allocation = 0
      expect(await lpContract.tokenAllocationOf(account1.address)).to.equal(0)

      const allocateETH = ethers.utils.parseEther("1")
      await lpContract.setTokenAllocation(account1.address, allocateETH)

      //account1 now has 1 ether allocation
      expect(await lpContract.tokenAllocationOf(account1.address)).to.equal(allocateETH)

      const leaf = keccak256(account1.address)
      const proof = tree.getHexProof(leaf)
      const buyETH = ethers.utils.parseEther("0.5")
      
      //buy token
      await lpContract.connect(account1).buyToken(proof, { value: buyETH })

      // 0.5/0.03 = 16
      let expected = BigNumber.from(16).mul(BigNumber.from(10).pow(await tokenContract.decimals()))
      expect(await tokenContract.balanceOf(account1.address)).to.equal(expected)

      //should spent half of allocation
      expect(await lpContract.tokenClaimedOf(account1.address)).to.equal(buyETH)

      //buy again
      await lpContract.connect(account1).buyToken(proof, { value: buyETH })
      expected = BigNumber.from(32).mul(BigNumber.from(10).pow(await tokenContract.decimals()))
      expect(await tokenContract.balanceOf(account1.address)).to.equal(expected)

      //should spent all of allocation
      expect(await lpContract.tokenClaimedOf(account1.address)).to.equal(allocateETH)

      //can't buy more 
      await expect(lpContract.connect(account1).buyToken(proof, { value: utils.parseEther("0.1") })).to.revertedWith("Allocation limited.")
    })

    it("Whitelisted should be able to buy nft within allocation limit", async function () {
      //account1 allocation = 0
      expect(await lpContract.nftAllocationOf(account1.address)).to.equal(0)

      await lpContract.setNftAllocation(account1.address, 3)

      //account1 now has 3 nfts allocation
      expect(await lpContract.nftAllocationOf(account1.address)).to.equal(3)

      const leaf = keccak256(account1.address)
      const proof = tree.getHexProof(leaf)

      //buy 2 nfts, 0.2 eth each
      await lpContract.connect(account1).buyNft(proof, 2, { value: utils.parseEther("0.4") })

      expect(await nftContract.balanceOf(account1.address)).to.equal(2)

      //should spent 2 nfts allocation
      expect(await lpContract.nftClaimedOf(account1.address)).to.equal(2)

      //buy 1 more
      await lpContract.connect(account1).buyNft(proof, 1, { value: utils.parseEther("0.2") })
      expect(await nftContract.balanceOf(account1.address)).to.equal(3)

      //should spent all of allocation
      expect(await lpContract.nftClaimedOf(account1.address)).to.equal(3)

      //can't buy more 
      await expect(lpContract.connect(account1).buyToken(proof, { value: utils.parseEther("0.2") })).to.revertedWith("Allocation limited.")
    })

    it("Non-Whitelisted shoudn't be able to buy token or nft", async function () {
      const leaf = keccak256(account3.address)
      const proof = tree.getHexProof(leaf)
      const buyETH = ethers.utils.parseEther("0.4")

      //buy token
      await expect(lpContract.connect(account3).buyToken(proof, { value: buyETH })).to.be.revertedWith("Invalid Proof.")

      //buy nft
      await expect(lpContract.connect(account3).buyNft(proof, 2, { value: buyETH })).to.be.revertedWith("Invalid Proof.")
    })

    it("Can't buy before sale start", async function () {
      const startDate = new Date()
      startDate.setDate(startDate.getDate() + 7)
      const endDate = new Date()
      endDate.setDate(endDate.getDate() + 14)

      const startUnix = Math.floor(startDate.getTime() / 1000)
      const endUnix = Math.floor(endDate.getTime() / 1000)

      await lpContract.setSaleDate(startUnix, endUnix)

      const leaf = keccak256(account1.address)
      const proof = tree.getHexProof(leaf)
      const buyETH = ethers.utils.parseEther("0.4")

      //buy token
      await expect(lpContract.connect(account1).buyToken(proof, { value: buyETH })).to.be.revertedWith("Sale not start yet.")

      //buy nft
      await expect(lpContract.connect(account1).buyNft(proof, 2, { value: buyETH })).to.be.revertedWith("Sale not start yet.")
    })

    it("Can't buy after sale ended", async function () {
      const startDate = new Date()
      startDate.setDate(startDate.getDate() - 14)
      const endDate = new Date()
      endDate.setDate(endDate.getDate() - 7)

      const startUnix = Math.floor(startDate.getTime() / 1000)
      const endUnix = Math.floor(endDate.getTime() / 1000)

      await lpContract.setSaleDate(startUnix, endUnix)

      const leaf = keccak256(account1.address)
      const proof = tree.getHexProof(leaf)
      const buyETH = ethers.utils.parseEther("0.4")

      //buy token
      await expect(lpContract.connect(account1).buyToken(proof, { value: buyETH })).to.be.revertedWith("Sale ended.")

      //buy nft
      await expect(lpContract.connect(account1).buyNft(proof, 2, { value: buyETH })).to.be.revertedWith("Sale ended.")
    })
  })
});
