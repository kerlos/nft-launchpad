import express from 'express';
import 'dotenv/config'
import VerifyRoutes from './src/controllers/verify';
import AdminRoutes from './src/controllers/admin';
import LaunchpadRoutes from './src/controllers/launchpad';
import cors from 'cors'
const app = express();
var allowedOrigins = ['http://localhost:3000', 'https://lp.kerlos.in.th'];
const port = process.env.APP_PORT;
app.use(express.json())

app.use(cors({
  origin: function(origin, callback){
    if(!origin) return callback(null, true);
    if(allowedOrigins.indexOf(origin) === -1){
      var msg = 'The CORS policy for this site does not ' +
                'allow access from the specified Origin.';
      return callback(new Error(msg), false);
    }
    return callback(null, true);
  }
}));

VerifyRoutes(app)
AdminRoutes(app)
LaunchpadRoutes(app)

app.listen(port, () => {
    console.log(`application is running on port ${port}.`);
});