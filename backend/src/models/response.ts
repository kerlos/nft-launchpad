type APIResponseData<T> = {
    success: boolean
    data: T | null
    message: string | null
}
function APIResponse<T> (success: boolean, message?: string | null, data?: T): APIResponseData<T> {
    return {
        success: success,
        data: data ? data : null,
        message: message ? message : null
    }
}

export { APIResponseData, APIResponse }