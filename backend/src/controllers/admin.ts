import express, { Request, Response } from 'express'
import VerifyService from '../services/verify'
import { APIResponse, APIResponseData } from '../models/response'
import LaunchpadService from '../services/launchpad'
import launchpadData from '../models/launchpadData'

const verifySvc = new VerifyService()
const lpSvc = new LaunchpadService()
function Routes (app: express.Application) {
    app.post('/admin/launchpad/update', async (req: Request, resp: Response<APIResponseData<null>>) => {
        try {
            await verifySvc.setData()
            return resp.json(APIResponse(true))
        } catch (e) {
            return resp.json(APIResponse(false, (e as Error).message))
        }
    })

    app.get('/admin/launchpad/get', async (req: Request, resp: Response<APIResponseData<launchpadData>>) => {
        try {
            const respData = APIResponse<launchpadData>(true)
            const wl = await lpSvc.GetWhitelists()
            let root = await verifySvc.getRoot()
            if (root === null) {
                await verifySvc.setData()
                root = await verifySvc.getRoot()
            }
            respData.data = {
                root: root!,
                whitelists: wl
            }
            return resp.json(respData)
        } catch (e) {
            return resp.json(APIResponse(false, (e as Error).message))
        }
    })


}

export default Routes