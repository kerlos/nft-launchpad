import express, { Request, Response } from 'express'

import LaunchpadService from '../services/launchpad'
import { APIResponse, APIResponseData } from '../models/response'
import { MessageTypes, TypedMessage } from '@metamask/eth-sig-util'

const lpSvc = new LaunchpadService()
function Routes (app: express.Application) {

    app.get('/launchpad/register/status/:address', async (req: Request, resp: Response<APIResponseData<boolean>>) => {
        try {
            const respData = APIResponse<boolean>(false)
            const address: string = req.params.address
            if (address.length === 0) {
                respData.message = "invalid address"
                return resp.json(respData)
            }
            const registered = await lpSvc.RegisterStatus(address)
            respData.data = registered
            respData.success = true
            return resp.json(respData)

        } catch (e) {
            return resp.json(APIResponse(false, (e as Error).message))
        }
    })

    app.post('/launchpad/register/request', async (req: Request, resp: Response<APIResponseData<TypedMessage<MessageTypes>>>) => {
        try {
            const respData = APIResponse<TypedMessage<MessageTypes>>(false)
            const msg = await lpSvc.RegisterRequest()
            respData.data = msg
            respData.success = true
            return resp.json(respData)
        } catch (e) {
            return resp.json(APIResponse(false, (e as Error).message))
        }
    })

    app.post('/launchpad/register/validate', async (req: Request, resp: Response<APIResponseData<null>>) => {
        try {
            const respData = APIResponse<null>(false)
            const address: string = req.body.address
            if (address.length === 0) {
                respData.message = "invalid address"
                return resp.json(respData)
            }

            const message: TypedMessage<MessageTypes> | null = req.body.message ? req.body.message : null
            if (message === null) {
                respData.message = "invalid message"
                return resp.json(respData)
            }

            const signature: string = req.body.signature
            if (signature.length === 0) {
                respData.message = "invalid signature"
                return resp.json(respData)
            }
            const success = await lpSvc.RegisterValidate(address, message!, signature)
            if (success === false) {
                respData.message = "validate failed"
                return resp.json(respData)
            }
            respData.success = true
            return resp.json(respData)
        } catch (e) {
            return resp.json(APIResponse(false, (e as Error).message))
        }
    })
}

export default Routes