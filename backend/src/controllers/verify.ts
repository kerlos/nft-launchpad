import express, { Request, Response } from 'express'
import { PrismaClient } from '@prisma/client'
import VerifyService from '../services/verify'
import { APIResponse, APIResponseData } from '../models/response'

const verifySvc = new VerifyService()
function Routes (app: express.Application) {
    app.get('/verify/getproof/:address', async (req: Request, resp: Response<APIResponseData<string[]>>) => {
        const respData = APIResponse<string[]>(false)
        const address: string = req.params.address
        if (address.length === 0) {
            respData.message = "invalid address"
            return resp.json(respData)
        }
        const data = await verifySvc.getProof(address)
        if (data === null) {
            respData.message = "not available"
            return resp.json(respData)
        }
        respData.data = data
        respData.success = true
        return resp.json(respData)
    })
}

export default Routes