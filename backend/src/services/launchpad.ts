import { PrismaClient, Whitelist } from '@prisma/client';
import { MessageTypes, recoverTypedSignature, SignTypedDataVersion, TypedMessage } from "@metamask/eth-sig-util"
import { toChecksumAddress } from "ethereumjs-util"
import { ethers } from "ethers"
import { lpAddress } from "../../data/addresses"

const prisma = new PrismaClient()

function toHexString (byteArray: Uint8Array) {
    var s = '0x';
    byteArray.forEach(function (byte) {
        s += ('0' + (byte & 0xFF).toString(16)).slice(-2);
    });
    return s;
}
class LaunchpadService {
    async RegisterRequest (): Promise<TypedMessage<MessageTypes>> {
        const msg: TypedMessage<MessageTypes> = {
            types: {
                EIP712Domain: [
                    { name: "name", type: "string" },
                    { name: "version", type: "string" },
                    { name: "chainId", type: "uint256" },
                    { name: "verifyingContract", type: "address" },
                ],
                Sign: [
                    { name: "contents", type: "string" }
                ]
            },
            primaryType: "Sign",
            domain: {
                chainId: 97, //bsc testnet
                name: "GuyFriend Launchpad",
                verifyingContract: lpAddress,
                version: "1"
            },
            message: {
                contents: toHexString(ethers.utils.randomBytes(16))
            }
        }
        return msg
    }
    async RegisterValidate (address: string, data: TypedMessage<MessageTypes>, signature: string): Promise<boolean> {
        const recoverAddress = recoverTypedSignature({ data, signature, version: SignTypedDataVersion.V4 })
        const valid = toChecksumAddress(address) === toChecksumAddress(recoverAddress)

        if (valid) {
            const record = await prisma.whitelist.create({
                data: {
                    Address: address,
                }
            })
        }
        return valid
    }

    async RegisterStatus (address: string): Promise<boolean> {
        const record = await prisma.whitelist.findFirst({
            where: {
                Address: address
            }
        })
        return record !== null
    }

    async GetWhitelists (): Promise<Whitelist[]> {
        const whitelists = await prisma.whitelist.findMany({
            where: {
                Enabled: true
            }
        })

        return whitelists
    }
}

export default LaunchpadService