import { MerkleTree } from "merkletreejs"
import keccak256 from "keccak256"
import "dotenv/config"
import KeyvPostgres from "@keyvhq/postgres"
import Keyv from "keyv"
import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

const keyv = new Keyv({ store: new KeyvPostgres(process.env.DATABASE_URL) })

class VerifyService {
    async getTree (): Promise<MerkleTree | null> {
        const leavesJSON = await keyv.get('verify:leaves')
        if (typeof leavesJSON === 'undefined') {
            return null
        }
        const leaves = MerkleTree.unmarshalLeaves(leavesJSON)
        const tree = new MerkleTree(leaves, keccak256, { sort: true })
        return tree
    }

    async getRoot (): Promise<string | null> {
        const root = await keyv.get('verify:root')
        return root ? root : null
    }

    async getProof (address: string): Promise<string[] | null> {
        const tree = await this.getTree()
        if (tree === null) {
            return null
        }
        const leaf = keccak256(address)
        const proof = tree.getHexProof(leaf)
        return proof
    }

    async setData () {
        const result = await prisma.whitelist.findMany({
            where: {
                Enabled: true
            }
        })
        const leaves = result.map(x => keccak256(x.Address))
        const tree = new MerkleTree(leaves, keccak256, { sort: true })
        const root = tree.getHexRoot()
        await keyv.set('verify:root', root)
        await keyv.set('verify:leaves', MerkleTree.marshalLeaves(leaves))
    }
}

export default VerifyService