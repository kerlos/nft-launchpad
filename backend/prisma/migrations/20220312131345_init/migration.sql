-- CreateTable
CREATE TABLE "Whitelist" (
    "id" SERIAL NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "Address" VARCHAR(255) NOT NULL,
    "TokenAllocate" DOUBLE PRECISION NOT NULL DEFAULT 0.5,
    "NftAllocate" INTEGER NOT NULL DEFAULT 3,

    CONSTRAINT "Whitelist_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Whitelist_Address_key" ON "Whitelist"("Address");
