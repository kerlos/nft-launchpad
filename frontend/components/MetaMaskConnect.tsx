import { useWeb3, useSwitchNetwork } from "@3rdweb/hooks";
import { Button, Menu, MenuButton, MenuItem, MenuList } from "@chakra-ui/react";
import { HiChevronDown } from "react-icons/hi";
import { IoMdWallet } from "react-icons/io";
import { useState } from "react";

const MetaMaskConnect = () => {
  const web3 = useWeb3();
  const [switching, setSwitching] = useState<boolean>(false);
  const {
    address,
    chainId,
    connectWallet,
    disconnectWallet,
    getNetworkMetadata,
    provider,
  } = web3;
  const { switchNetwork, canAttemptSwitch } = useSwitchNetwork();

  function trimAddress(address: string) {
    const start = address.substring(0, 6);
    const end = address.substring(address.length - 4);
    return `${start}...${end}`;
  }
  let networkMetadata = null;

  if (chainId) {
    if (switching) {
      setSwitching(false);
    }
    //console.log('get network meta')
    networkMetadata = getNetworkMetadata(chainId);
  }
  if (!switching && canAttemptSwitch && chainId !== 97) {
    setSwitching(true);
    switchNetwork(97);
  }
  async function connectWalletAndSwitchNetwork() {
    await connectWallet("injected");
  }
  return (
    <>
      {address ? (
        <Menu>
          <MenuButton minH={10} as={Button} rightIcon={<HiChevronDown />}>
            {trimAddress(address)}
          </MenuButton>
          <MenuList>
            <MenuItem onClick={disconnectWallet}>Switch Wallet</MenuItem>
          </MenuList>
        </Menu>
      ) : (
        <>
          <Button
            onClick={connectWalletAndSwitchNetwork}
            leftIcon={<IoMdWallet />}
            colorScheme="facebook"
          >
            Connect MetaMask
          </Button>
        </>
      )}
    </>
  );
};

export default MetaMaskConnect;
