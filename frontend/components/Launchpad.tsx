import LaunchpadData from "../types/launchpadData";
import Web3Client from "../lib/Web3Client";
import { useWeb3, useSwitchNetwork } from "@3rdweb/hooks";
import {
  Table,
  Thead,
  Tbody,
  Tfoot,
  Tr,
  Th,
  Td,
  TableCaption,
  Spacer,
  Flex,
  Box,
  Center,
  Button,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
  Text,
  Badge,
} from "@chakra-ui/react";
import { BigNumber, ethers, EventFilter } from "ethers";
import {
  GetProof,
  RegisterRequest,
  RegisterStatus,
  RegisterValidate,
} from "../lib/api";
import { useState } from "react";

const Launchpad = (props: {
  launchpad: LaunchpadData;
  web3Client: Web3Client;
}) => {
  function formatNumber(n: BigNumber) {
    return ethers.utils.formatEther(n);
  }
  
  const [registered, setRegister] = useState(false);
  const { launchpad, web3Client } = props;
  const tokenAllocation = parseFloat(formatNumber(launchpad.TokenAllocation!));
  const tokenClaimed = parseFloat(formatNumber(launchpad.TokenClaimed!));
  const nftAllocation = parseInt(launchpad.NftAllocation!.toString());
  const nftClaimed = parseInt(launchpad.NftClaimed!.toString());
  const tokenPrice = parseFloat(formatNumber(launchpad.TokenPrice!));
  const nftPrice = parseFloat(formatNumber(launchpad.NftPrice!));
  let tokenBuyAmount = 0;
  let nftBuyAmount = 0;
  const buyAmount = 0;

  const web3 = useWeb3();
  const { address, chainId, provider } = web3;
  const ethereum = (window as any).ethereum;

  const [loading, setLoading] = useState(true);

  async function register() {
    try {
      const msg = await RegisterRequest();
      const signature = await ethereum.request({
        method: "eth_signTypedData_v4",
        params: [address, JSON.stringify(msg)],
      });

      const success = await RegisterValidate(address!, msg, signature);
      if (success) {
        setRegister(true);
      }
    } catch {
      console.log("not sign");
    }
  }

  async function buyToken() {
    console.log("buyToken");
    const payAmount = ethers.utils.parseEther(tokenBuyAmount.toString());
    const proof = await GetProof(address!);

    if (proof.length > 0) {
      await web3Client!.lpContract
        .connect(provider!.getSigner(address))
        .buyToken(proof, {
          value: payAmount,
        });
    }
  }
  async function buyNft() {
    console.log("buyNft");
    const total = nftBuyAmount * nftPrice;
    const payAmount = ethers.utils.parseEther(total.toString());
    const proof = await GetProof(address!);

    if (proof.length > 0) {
      await web3Client!.lpContract
        .connect(provider!.getSigner(address))
        .buyNft(proof, nftBuyAmount, {
          value: payAmount,
        });
    }
  }

  const onBuyTokenChange = (e: React.FocusEvent<HTMLInputElement>) => {
    if (e.target.value.length > 0) {
      try {
        tokenBuyAmount = parseFloat(e.target.value);
      } catch {
        tokenBuyAmount = 0;
      }
    }
  };

  const onBuyNftChange = (e: React.FocusEvent<HTMLInputElement>) => {
    if (e.target.value.length > 0) {
      try {
        nftBuyAmount = parseFloat(e.target.value);
        //console.log('change to ',nftBuyAmount)
      } catch {
        nftBuyAmount = 0;
      }
    }
  };

  if (address && loading) {
    setLoading(false);
    console.log("listen");
    RegisterStatus(address).then((res) => {
      setRegister(res);
      // console.log("reg", res);
    });
  }
  return (
    <>
      <Flex direction="column">
        <Box m="2">
          <Center>
            {registered ? (
              <Badge colorScheme="green">Registered</Badge>
            ) : (
              <Button onClick={register}>Register</Button>
            )}
          </Center>
        </Box>
        <Box borderWidth="1px" borderRadius="lg" p="2">
          <Table variant="simple">
            <Thead>
              <Tr>
                <Th>Sale Date</Th>
                <Th></Th>
              </Tr>
            </Thead>
            <Tbody>
              <Tr>
                <Td>StartDate</Td>
                <Td>{launchpad.StartDate!.toString()}</Td>
              </Tr>
              <Tr>
                <Td>EndDate</Td>
                <Td>{launchpad.EndDate!.toString()}</Td>
              </Tr>
            </Tbody>
          </Table>
        </Box>

        <Box mt="2">
          <Flex direction="row">
            <Box borderWidth="1px" borderRadius="lg" mr="2" p="2">
              <Table variant="simple">
                <Thead>
                  <Tr>
                    <Th>Token Presale</Th>
                    <Th></Th>
                  </Tr>
                </Thead>
                <Tbody>
                  <Tr>
                    <Td>Price / Token</Td>
                    <Td>{tokenPrice} BNB</Td>
                  </Tr>
                  <Tr>
                    <Td>Your Allocation</Td>
                    <Td>
                      {tokenAllocation - tokenClaimed}/{tokenAllocation} BNB
                    </Td>
                  </Tr>
                </Tbody>
              </Table>
              <Center mt="2">
                <NumberInput
                  onBlur={onBuyTokenChange}
                  inputMode="decimal"
                  step={0.001}
                  min={0}
                  max={tokenAllocation - tokenClaimed}
                  mr="2"
                >
                  <NumberInputField placeholder="Amount" step={0.001} />
                  <NumberInputStepper>
                    <NumberIncrementStepper />
                    <NumberDecrementStepper />
                  </NumberInputStepper>
                </NumberInput>
                <Button
                  colorScheme="blue"
                  disabled={tokenAllocation - tokenClaimed <= 0}
                  onClick={buyToken}
                >
                  Buy
                </Button>
              </Center>
            </Box>

            <Box borderWidth="1px" borderRadius="lg" p="2">
              <Table variant="simple">
                <Thead>
                  <Tr>
                    <Th>NFT Presale</Th>
                    <Th></Th>
                  </Tr>
                </Thead>
                <Tbody>
                  <Tr>
                    <Td>Price / NFT</Td>
                    <Td>{nftPrice} BNB</Td>
                  </Tr>
                  <Tr>
                    <Td>Your Allocation</Td>
                    <Td>
                      {nftAllocation - nftClaimed}/{nftAllocation} Nft(s)
                    </Td>
                  </Tr>
                </Tbody>
              </Table>
              <Center mt="2">
                <NumberInput
                  step={1}
                  min={0}
                  max={nftAllocation - nftClaimed}
                  mr="2"
                  onBlur={onBuyNftChange}
                >
                  <NumberInputField placeholder="Amount" />
                  <NumberInputStepper>
                    <NumberIncrementStepper />
                    <NumberDecrementStepper />
                  </NumberInputStepper>
                </NumberInput>
                <Button
                  colorScheme="purple"
                  disabled={nftAllocation - nftClaimed <= 0}
                  onClick={buyNft}
                >
                  Buy
                </Button>
              </Center>
            </Box>
          </Flex>
        </Box>
      </Flex>
    </>
  );
};

export default Launchpad;
