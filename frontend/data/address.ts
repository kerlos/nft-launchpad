
const tokenAddress = "0xDDbD220769A7947e305c2D1A25cE17F5A8B8EEC7"
const nftAddress = "0xad9E4ac736AB70072bEB8D5D9f2afF4c52a64575"
const lpAddress = "0x2B21F9887cbBFF74Da732BE8346a969FE8b555ee"

export {
    nftAddress,
    tokenAddress,
    lpAddress
}