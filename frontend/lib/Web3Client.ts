import { BigNumber, Contract, providers } from "ethers";
import { nftAddress, tokenAddress, lpAddress } from "../data/address";
import nftABI from "../data/abi/GuyFriendNFT.json";
import tokenABI from "../data/abi/GuyFriendToken.json";
import lpABI from "../data/abi/GFLaunchpad.json";
import LaunchpadData from "../types/launchpadData";

class Web3Client {
    address: string
    tokenContract: Contract
    nftContract: Contract
    lpContract: Contract

    constructor(address: string, provider: providers.Web3Provider) {
        this.nftContract = new Contract(nftAddress, nftABI, provider);
        this.tokenContract = new Contract(tokenAddress, tokenABI, provider);
        this.lpContract = new Contract(lpAddress, lpABI, provider);
        this.address = address;
    }

    GetTokenBalance (): Promise<BigNumber> {
        return this.tokenContract.balanceOf(this.address);
    }

    GetNftBalance (): Promise<BigNumber> {
        return this.nftContract.balanceOf(this.address);
    }

    async GetLaunchpad (): Promise<LaunchpadData> {
        const [
            startDate,
            endDate,
            tokenAlloc,
            tokenClaim,
            tokenPrice,
            nftAlloc,
            nftClaim,
            nftPrice,
        ] = await Promise.all([
            this.lpContract.startDate(),
            this.lpContract.endDate(),
            this.lpContract.tokenAllocationOf(this.address),
            this.lpContract.tokenClaimedOf(this.address),
            this.lpContract.tokenPrice(),
            this.lpContract.nftAllocationOf(this.address),
            this.lpContract.nftClaimedOf(this.address),
            this.lpContract.nftPrice(),
        ]);
        return {
            StartDate: new Date(startDate.toNumber() * 1000),
            EndDate: new Date(endDate.toNumber() * 1000),
            TokenAllocation: tokenAlloc,
            TokenClaimed: tokenClaim,
            TokenPrice: tokenPrice,
            NftAllocation: nftAlloc,
            NftClaimed: nftClaim,
            NftPrice: nftPrice,
        };
    }

    // async BuyToken(amount: number) {
    //     this.lpContract.buyToken()
    // }
}

export default Web3Client;