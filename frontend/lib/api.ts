
import APIResponseData from "../types/apiResponseData"
import axios from "axios"

const baseURL = process.env.NEXT_PUBLIC_API_HOST
async function RegisterRequest (): Promise<any> {
    const url = `${baseURL}/launchpad/register/request`

    const res = await axios.post<APIResponseData>(url)
    return res.data.data
}

async function RegisterValidate (address: string, msg: any, signature: string): Promise<boolean> {
    const url = `${baseURL}/launchpad/register/validate`
    const res = await axios.post<APIResponseData>(url, {
        address: address,
        message: msg,
        signature: signature
    })
    return res.data.success
}

async function RegisterStatus (address: string): Promise<boolean> {
    const url = `${baseURL}/launchpad/register/status/${address}`
    const res = await axios.get<APIResponseData>(url)
    return res.data.data
}

async function GetProof (address: string): Promise<string[]> {
    const url = `${baseURL}/verify/getproof/${address}`
    const res = await axios.get<APIResponseData>(url)
    return res.data.data
}

async function UpdateTree(): Promise<void> {
    const url = `${baseURL}/admin/launchpad/update`
    const res = await axios.post<APIResponseData>(url)
    return res.data.data
}

async function AdminData (): Promise<any> {
    const url = `${baseURL}/admin/launchpad/get`
    const res = await axios.get<APIResponseData>(url)
    return res.data.data
}

export { RegisterRequest, RegisterValidate, RegisterStatus, AdminData, GetProof, UpdateTree }