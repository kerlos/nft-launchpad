type APIResponseData = {
    success: boolean
    data: any
    message: string | null
}

export default APIResponseData