import { BigNumber } from "ethers"

type LaunchpadData = {
    StartDate: Date | null
    EndDate: Date | null
    TokenPrice: BigNumber | null
    TokenAllocation: BigNumber | null
    TokenClaimed: BigNumber | null
    NftPrice: BigNumber | null
    NftAllocation: BigNumber | null
    NftClaimed: BigNumber | null
}

export default LaunchpadData