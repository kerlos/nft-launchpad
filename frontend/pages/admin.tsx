import { useWeb3 } from "@3rdweb/hooks";
import {
  Box,
  Button,
  Container,
  Flex,
  FormControl,
  FormHelperText,
  FormLabel,
  Input,
  NumberDecrementStepper,
  NumberIncrementStepper,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  Table,
  TableCaption,
  Tbody,
  Td,
  Text,
  Tfoot,
  Center,
  Th,
  Thead,
  Tr,
} from "@chakra-ui/react";
import { JsonRpcSigner } from "@ethersproject/providers";
import { BigNumber, ethers } from "ethers";
import { NextPage } from "next";
import Head from "next/head";
import React from "react";
import { useEffect, useState } from "react";
import DtPicker, { Day } from "react-calendar-datetime-picker";
import "react-calendar-datetime-picker/dist/index.css";
import { start } from "repl";
import { AdminData, UpdateTree } from "../lib/api";
import Web3Client from "../lib/Web3Client";
import LaunchpadData from "../types/launchpadData";

const Admin: NextPage = () => {
  const [lanchpad, setlaunchpad] = useState<LaunchpadData | null>(null);

  const [
    { startDate, endDate, tokenPrice, nftPrice, root, whitelists },
    setStates,
  ] = useState<{
    startDate: Day;
    endDate: Day;
    tokenPrice: number;
    nftPrice: number;
    root: string;
    whitelists: any[];
  }>({
    startDate: null,
    endDate: null,
    tokenPrice: 0,
    nftPrice: 0,
    root: "",
    whitelists: [],
  });
  function test() {
    console.log(startDate);
  }
  let lpContract: ethers.Contract;
  const { address, provider } = useWeb3();
  const [loading, setLoading] = useState(true);
  let web3Client: Web3Client | null = null;

  if (address) {
    web3Client = new Web3Client(address, provider!);
    lpContract = web3Client!.lpContract.connect(provider!.getSigner());
    if (loading) {
      setLoading(false);
      loadData();
    }
  }

  function setStartDate(val: any) {
    setStates({
      startDate: val,
      endDate: endDate,
      tokenPrice: tokenPrice,
      nftPrice: nftPrice,
      root: root,
      whitelists: whitelists,
    });
  }

  function setEndDate(val: any) {
    setStates({
      startDate: startDate,
      endDate: val,
      tokenPrice: tokenPrice,
      nftPrice: nftPrice,
      root: root,
      whitelists: whitelists,
    });
  }

  async function loadData() {
    console.log("load data");
    const [lp, adminData] = await Promise.all([
      web3Client!.GetLaunchpad(),
      AdminData(),
    ]);

    setlaunchpad(lp);
    const std = lp.StartDate;
    const etd = lp.EndDate;
    const st: Day = {
      day: std!.getDay(),
      month: std!.getMonth() + 1,
      year: std!.getFullYear(),
      hour: std!.getHours(),
      minute: std!.getMinutes(),
    };
    const ed: Day = {
      day: etd!.getDay(),
      month: etd!.getMonth() + 1,
      year: etd!.getFullYear(),
      hour: etd!.getHours(),
      minute: etd!.getMinutes(),
    };

    const tkp = parseFloat(ethers.utils.formatEther(lp.TokenPrice!));
    const np = parseFloat(ethers.utils.formatEther(lp.NftPrice!));

    setStates({
      startDate: st,
      endDate: ed,
      tokenPrice: tkp,
      nftPrice: np,
      root: adminData.root,
      whitelists: adminData.whitelists,
    });
  }

  function DayToDate(d: Day): Date {
    return new Date(d!.year, d!.month - 1, d!.day, d!.hour, d!.minute, 0);
  }

  function DateToUnix(d: Date): number {
    return Math.floor(d.getTime() / 1000);
  }

  async function updateDate() {
    const startUnix = DateToUnix(DayToDate(startDate));
    const endUnix = DateToUnix(DayToDate(endDate));
    await lpContract.setSaleDate(startUnix, endUnix);
  }

  function setTokenPrice(valueAsString: string, valueAsNumber: number) {
    setStates({
      startDate: startDate,
      endDate: endDate,
      tokenPrice: valueAsNumber,
      nftPrice: nftPrice,
      root: root,
      whitelists: whitelists,
    });
  }

  function setNftPrice(valueAsString: string, valueAsNumber: number) {
    setStates({
      startDate: startDate,
      endDate: endDate,
      tokenPrice: tokenPrice,
      nftPrice: valueAsNumber,
      root: root,
      whitelists: whitelists,
    });
  }

  async function updateTokenPrice() {
    const tpWei = ethers.utils.parseEther(tokenPrice.toString());
    await lpContract.setTokenPrice(tpWei);
  }

  async function updateNftPrice() {
    const npWei = ethers.utils.parseEther(nftPrice.toString());
    await lpContract.setNftPrice(npWei);
  }

  async function updateRoot() {
    await lpContract.setMerkleRoot(root);
  }

  async function updateTree() {
    await UpdateTree();
  }

  async function updateWhitelist() {
    const addrArr: string[] = [];
    const tokenArr: BigNumber[] = [];
    const nftArr: number[] = [];
    for (let i = 0; i < whitelists.length; i++) {
      addrArr.push(whitelists[i].Address);
      tokenArr.push(
        ethers.utils.parseEther(whitelists[i].TokenAllocate.toString())
      );
      nftArr.push(whitelists[i].NftAllocate);
    }
    await Promise.all([
      lpContract.batchSetTokenAllocation(addrArr, tokenArr),
      lpContract.batchSetNftAllocation(addrArr, nftArr),
    ]);
  }

  return (
    <>
      <Head>
        <title>GuyFriend Admin</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Container maxW="container.md" mt={4}>
        <Flex flexDirection="column" w="100%" h="10%">
          <Box borderWidth="1px" borderRadius="lg" p="4">
            <Flex flexDir="row">
              <Text mr="2" w="30%">
                Start DateTime
              </Text>
              <DtPicker
                withTime={true}
                autoClose={false}
                showTimeInput={true}
                initValue={startDate}
                onChange={setStartDate}
              />
            </Flex>
            <Flex flexDir="row" mt={2}>
              <Text mr="2" w="30%">
                End DateTime
              </Text>
              <DtPicker
                initValue={endDate}
                withTime={true}
                autoClose={false}
                showTimeInput={true}
                onChange={setEndDate}
              />
            </Flex>
            <Center mt={2}>
              <Button onClick={updateDate}>Update</Button>
            </Center>
          </Box>

          <Box borderWidth="1px" borderRadius="lg" p="4" mt="4">
            <Flex direction="column" gap={4}>
              <FormControl mr={2}>
                <FormLabel htmlFor="tokenPrice">Token Price (BNB)</FormLabel>
                <NumberInput
                  id="tokenPrice"
                  min={0}
                  step={0.001}
                  value={tokenPrice}
                  onChange={setTokenPrice}
                >
                  <NumberInputField />
                  <NumberInputStepper>
                    <NumberIncrementStepper />
                    <NumberDecrementStepper />
                  </NumberInputStepper>
                </NumberInput>
                <Center mt={2}>
                  <Button onClick={updateTokenPrice}>Update</Button>
                </Center>
              </FormControl>

              <FormControl>
                <FormLabel htmlFor="nftPrice">NFT Price (BNB)</FormLabel>
                <NumberInput
                  id="nftPrice"
                  min={0}
                  step={0.001}
                  value={nftPrice}
                  onChange={setNftPrice}
                >
                  <NumberInputField />
                  <NumberInputStepper>
                    <NumberIncrementStepper />
                    <NumberDecrementStepper />
                  </NumberInputStepper>
                </NumberInput>
                <Center mt={2}>
                  <Button onClick={updateNftPrice}>Update</Button>
                </Center>
              </FormControl>

              <FormControl>
                <FormLabel htmlFor="email">UpToDate Merkle Root</FormLabel>
                <Input type="text" value={root} readOnly />
                <Center mt={2}>
                  <Button onClick={updateTree} mr={2}>
                    Update Tree
                  </Button>
                  <Button onClick={updateRoot}>Update Root</Button>
                </Center>
              </FormControl>

              <FormLabel htmlFor="email">Whitelisted</FormLabel>
              <Table variant="simple">
                <Thead>
                  <Tr>
                    <Th>Address</Th>
                    <Th isNumeric>Token Allocate (BNB)</Th>
                    <Th isNumeric>Nft Allocate (Amount)</Th>
                  </Tr>
                </Thead>
                <Tbody>
                  {whitelists.map((el) => (
                    <Tr key={el.Address}>
                      <Td>{el.Address}</Td>
                      <Td isNumeric>{el.TokenAllocate}</Td>
                      <Td isNumeric>{el.NftAllocate}</Td>
                    </Tr>
                  ))}
                </Tbody>
              </Table>
              <Center mt={2}>
                <Button onClick={updateWhitelist}>Update</Button>
              </Center>
            </Flex>
          </Box>
        </Flex>
      </Container>
    </>
  );
};
export default Admin;
