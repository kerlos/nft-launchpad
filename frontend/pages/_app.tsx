import "../styles/globals.css";
import type { AppProps } from "next/app";
import { ThirdwebProvider } from "@3rdweb/react";
import { ChakraProvider } from "@chakra-ui/react";
import { AddEthereumChainParameter, NetworkMetadata } from "@3rdweb/hooks";
import { IoMdWallet } from "react-icons/io";

function MyApp({ Component, pageProps }: AppProps) {
  const supportedChainIds = [97];
  const connectors = {
    injected: {},
  };
  const networkMetas: { [key: number]: NetworkMetadata } = {
    97: {
      icon: IoMdWallet,
      chainName: "BSC Testnet",
      symbol: "BNB",
      isTestnet: true,
    },
  };

  const chainAddConfigs: { [key: number]: AddEthereumChainParameter } = {
    97: {
      chainId: "0x61",
      chainName: "BSC Testnet",
      nativeCurrency: {
        name: "Binance Coin",
        decimals: 18,
        symbol: "BNB",
      },
      rpcUrls: ["https://data-seed-prebsc-1-s1.binance.org:8545"],
      blockExplorerUrls: ["https://testnet.bscscan.com"],
    },
  };

  return (
    <ThirdwebProvider
      networkMetadata={networkMetas}
      connectors={connectors}
      supportedChainIds={supportedChainIds}
      chainAddConfig={chainAddConfigs}
    >
      <ChakraProvider>
        <Component {...pageProps} />
      </ChakraProvider>
    </ThirdwebProvider>
  );
}

export default MyApp;
