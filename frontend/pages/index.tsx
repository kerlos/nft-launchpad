import type { NextPage } from "next";
import Head from "next/head";
import {
  Container,
  Flex,
  Stat,
  StatLabel,
  StatNumber,
  toast,
  useToast,
} from "@chakra-ui/react";
import MetaMaskConnect from "../components/MetaMaskConnect";
import { useWeb3 } from "@3rdweb/hooks";
import { useRef, useState } from "react";
import { BigNumber, ethers } from "ethers";
import Launchpad from "../components/Launchpad";
import Web3Client from "../lib/Web3Client";
import LaunchpadData from "../types/launchpadData";
import { lpAddress } from "../data/address";
import { hexZeroPad } from "ethers/lib/utils";

const Home: NextPage = () => {
  const toast = useToast();
  const { address, provider } = useWeb3();
  const [{ tokenBalance, nftBalance, launchpad }, setStates] = useState<{
    tokenBalance: string;
    nftBalance: string;
    launchpad: LaunchpadData | null;
  }>({
    tokenBalance: "0",
    nftBalance: "0",
    launchpad: null,
  });

  const [loading, setLoading] = useState(true);
  const [listened, setListened] = useState(false);
  let web3Client: Web3Client | null = null;

  if (address) {
    web3Client = new Web3Client(address, provider!);
    if (loading) {
      setLoading(false);
      if (!listened) {
        setListened(true)
        web3Client!.lpContract.on(
          {
            address: lpAddress,
            topics: [
              ethers.utils.id("PurchaseToken(address,uint256)"),
              hexZeroPad(address, 32),
            ],
          },
          (address: string, amount: BigNumber) => {
            const amt = ethers.utils.formatEther(amount);
            toast.closeAll()
            toast({
              title: "Buy token Success",
              description: `Amount Received: ${amt} tokens`,
              status: "success",
              duration: 9000,
              isClosable: true,
            });
            loadData();
          }
        );
        web3Client!.lpContract.on(
          {
            address: lpAddress,
            topics: [
              ethers.utils.id("PurchaseNFT(address,uint256[])"),
              hexZeroPad(address, 32),
            ],
          },
          (address: string, ids: BigNumber[]) => {
            toast.closeAll()
            toast({
              title: "Buy NFT Success",
              description: `Amount Received: ${ids.length} NFT(s)`,
              status: "success",
              duration: 9000,
              isClosable: true,
            });
            loadData();
          }
        );
      }
      loadData();
    }
  }

  async function loadData() {
    console.log("load data");
    const [tokenBal, nftBal, lp] = await Promise.all([
      web3Client!.GetTokenBalance(),
      web3Client!.GetNftBalance(),
      web3Client!.GetLaunchpad(),
    ]);

    setStates({
      tokenBalance: ethers.utils.formatEther(tokenBal),
      nftBalance: nftBal.toString(),
      launchpad: lp,
    });
  }

  function launchpadProps() {
    return {
      launchpad: launchpad!,
      web3Client: web3Client!,
    };
  }

  return (
    <>
      <Head>
        <title>GuyFriend Launchpad</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Container maxW="container.md">
        <Flex flexDirection="column" w="100%" h="10%">
          <Flex justifyContent="flex-end" m={5}>
            <MetaMaskConnect />
          </Flex>
        </Flex>
        <Flex flexDirection="column" w="100%" h="90%">
          <Flex m={5}>
            <Stat>
              <StatLabel>Token Balance</StatLabel>
              <StatNumber>{tokenBalance}</StatNumber>
            </Stat>
            <Stat>
              <StatLabel>NFT Balance</StatLabel>
              <StatNumber>{nftBalance}</StatNumber>
            </Stat>
          </Flex>
          <Flex>
            {launchpad && web3Client && <Launchpad {...launchpadProps()} />}
          </Flex>
        </Flex>
      </Container>
    </>
  );
};

export default Home;
